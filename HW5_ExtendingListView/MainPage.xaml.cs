﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW5_ExtendingListView
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<InfinityStone> infinityStonesCollection = new ObservableCollection<InfinityStone>();

        public MainPage()
        {
            InitializeComponent();

            Title = "Infinity Stones";

            PopulateList();
        }

        private void PopulateList()
        {
            //var infinityStonesCollection = new ObservableCollection<InfinityStone>();

            var powerStone = new InfinityStone
            {
                Name = "Power Stone",
                Location = "Xandar",
                Description = "Allows the user to access and manipulate all forms of energy; " +
                    "enhance their physical strength and durability; " +
                    "enhance any superhuman ability; " +
                    "and boost the effects of the other five Stones. " +
                    "At full potential, the Power Stone grants the user omnipotence.",
                Picture = ImageSource.FromFile("PowerStone.png"),
                Color = Color.Purple,
            };

            var spaceStone = new InfinityStone
            {
                Name = "Space Stone",
                Location = "Asgard (Loki)",
                Description = "Allows the user to exist in any location; " +
                    "move any object anywhere throughout reality; " +
                    "warp or rearrange space; teleport themselves and others; " +
                    "increase their speed, and alter the distance between objects contrary to the laws of physics." +
                    "At full potential, the Space Stone grants the user omnipresence.",
                Picture = ImageSource.FromFile("SpaceStone.png"),
                Color = Color.SkyBlue,
            };

            var realityStone = new InfinityStone
            {
                Name = "Reality Stone",
                Location = "Knowhere",
                Description = "Allows the user to fulfil their wishes, " +
                    "even if the wish is in direct contradiction with scientific laws, " +
                    "and do things that would normally be impossible; " +
                    "and create any type of an alternate reality the user wishes. " +
                    "At full potential, when backed by the other Stones, " +
                    "the Reality Stone allows the user to alter reality on a universal scale.",
                Picture = ImageSource.FromFile("RealityStone.png"),
                Color = Color.Red,
            };

            var soulStone = new InfinityStone
            {
                Name = "Soul Stone",
                Location = "Vormir",
                Description = "Allows the user to steal, control, " +
                    "manipulate, and alter living and dead souls. " +
                    "The Soul Stone is also the gateway to an idyllic pocket universe." +
                    "At full potential, the Soul Stone grants the user control over all life in the universe.",
                Picture = ImageSource.FromFile("SoulStone.png"),
                Color = Color.DarkOrange,
            };

            var timeStone = new InfinityStone
            {
                Name = "Time Stone",
                Location = "Earth (Dr. Strange)",
                Description = "Allows the user to see into the past and the future; " +
                    "stop, slow down, speed up or reverse the flow of time; " +
                    "travel through time; change the past and the future; " +
                    "age and de-age beings, and trap people or entire universes in unending loops of time. " +
                    "At full potential, the Time Stone grants the user omniscience " +
                    "and total control over the past, present, and future.",
                Picture = ImageSource.FromFile("TimeStone.png"),
                Color = Color.Green,
            };

            var mindStone = new InfinityStone
            {
                Name = "Mind Stone",
                Location = "Earth (Vision)",
                Description = "Allows the user to enhance their mental and " +
                    "psionic abilities and access the thoughts and dreams of other beings. " +
                    "At full potential, when backed by the Power Stone, " +
                    "the Mind Stone can access all minds in existence simultaneously. " +
                    "The Mind Stone is also the manifestation of the universal subconscious.",
                Picture = ImageSource.FromFile("MindStone.png"),
                Color = Color.Yellow,
            };

            infinityStonesCollection.Add(powerStone);
            infinityStonesCollection.Add(spaceStone);
            infinityStonesCollection.Add(realityStone);
            infinityStonesCollection.Add(soulStone);
            infinityStonesCollection.Add(timeStone);
            infinityStonesCollection.Add(mindStone);

            InfinityStoneListView.ItemsSource = infinityStonesCollection;
        }

        void Handle_Clicked_Info(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var stone = (InfinityStone)menuItem.CommandParameter;
            Navigation.PushAsync(new InfoPage(stone));
        }

        void Handle_Clicked_Delete(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var stone = (InfinityStone)menuItem.CommandParameter;
            infinityStonesCollection.Remove(stone);
        }
    }
}
