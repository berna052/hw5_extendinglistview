﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HW5_ExtendingListView
{
    public partial class InfoPage : ContentPage
    {
        public InfoPage()
        {
            InitializeComponent();
        }

        public InfoPage(InfinityStone stone)
        {
            InitializeComponent();
            BindingContext = stone;
        }
    }
}
