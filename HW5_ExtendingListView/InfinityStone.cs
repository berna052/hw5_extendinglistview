﻿using System;
namespace HW5_ExtendingListView
{
    public class InfinityStone
    {

        public string Name
            {
                get;
                set;
            }

        public string Location
            {
                get;
                set;
            }

        public string Description
            {
                get;
                set;
            }

        public object Picture
            {
                get;
                set;
            }

        public object Color
            {
                get;
                set;
            }
    }
}
